@echo off
rem ****************************************************************************
rem * SCRIPT ACTS AS A WRAPPER TO THE MAIN 'UPDATE SUMMARY PDF' SCRIPT.
rem ****************************************************************************

rem Invoke the main 'update_summary_pdf.bat' script with:
rem   >> Paramter-01 == Path to <docs> folder.
rem   >> Paramter-02 == <blank> == Do NOT run in silent mode.
CALL <PATH_TO_DOXYGEN_GIT_DIRECTORY>\scripts\windows\update_summary_pdf.bat %~dp0\..\..



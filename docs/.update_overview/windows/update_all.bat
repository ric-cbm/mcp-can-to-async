@echo off
rem ****************************************************************************
rem * SCRIPT ACTS AS A WRAPPER TO THE MAIN 'UPDATE ALL' SCRIPT.
rem ****************************************************************************

rem Invoke the main 'update_all.bat' script with:
rem   >> Paramter-01 == Path to <docs> folder.
rem   >> Paramter-02 == Name of Doxyfile in <docs> folder.
rem   >> Paramter-03 == <blank> == Do NOT run in silent mode.
CALL <PATH_TO_DOXYGEN.GIT_DIRECTORY>\scripts\windows\update_all.bat %~dp0\..\.. Doxyfile.conf



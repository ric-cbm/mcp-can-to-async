
////////////////////////////////////////////////////////////////////////////////////////////////////
//!	@file
//!	
//!	@brief			This file contains the core of the can to async application. It manages invoking 
//!					standard-specific functions [ex: uart, rs232, rs485 half-duplex, rs485 full-duplex].
//!	
//!	@author			
////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef RS232_H
#define RS232_H


//==================================================================================================
// INCLUDES
//--------------------------------------------------------------------------------------------------

#include "core/can_to_async.h"


//==================================================================================================
// DEFINES
//--------------------------------------------------------------------------------------------------\

//Pin configurations for PIC32

//GEN_MODE_SWITCH:		switch between RS232 and RS485
//RECEIVER_ENABLE:		logic low enables reivers, logic high disables receivers
//DRIVER_ENABLE:		logic high enables drivers, logic low disables drivers
//TE485:				only matters for low power shutdown in RS232 mode, typically for RS485
//LOOPBACK:				logic high enables loopback mode, logic low disables
//RECEIVER_OUTPUT:		receiver output 1 for RS232
//DRIVER_INPUT:			driver input 1 for RS232
//FAST_ENABLE:			in fast mode DC-DC converter is active independent of driver,receiver, and termination states



//#define SET_TO_RS232()			TRISBbits.TRISB15			=INPUT		//this is for resistor indication of which mode LTC2870 should be in (app layer only)

#define UART3_INTERRUPT_VECTOR		31							
//#define ENABLE_UART3()					(U1MODE						=0x8085)	(already defined)
#define SET_BAUD_RATE_3()				(U3BRG						=BAUD_UART)	
#define ENABLE_INTERRUPT_UART3()		(IEC1bits.U3RXIE				=1)		//may be U2RXIE
#define INTERRUPT_PRIORITY_3()			(IPC8bits.U2IP				=5)	


//==================================================================================================
// TYPEDEFS
//--------------------------------------------------------------------------------------------------






//==================================================================================================
// FUNCTIONS
//--------------------------------------------------------------------------------------------------

void RS232_Low_Power_Shutdown();
void RS232_Drivers_On();
void RS232_Receivers_On();
void RS232_Loopback();
void Fast_Enable();
int Hut_RS232();


#endif
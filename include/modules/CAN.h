////////////////////////////////////////////////////////////////////////////////////////////////////
//!	@file
//!	
//!	@brief			This file contains the core of the can to async application. It manages invoking 
//!					standard-specific functions [ex: uart, rs232, rs485 half-duplex, rs485 full-duplex].
//!	
//!	@author			
////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef CAN_H
#define CAN_H


//==================================================================================================
// INCLUDES
//--------------------------------------------------------------------------------------------------

#include "core/can_to_async.h"
#include "lib_cbm_c32.h"


//==================================================================================================
// DEFINES
//--------------------------------------------------------------------------------------------------\

//CAN pin configuration on PIC32

//HW enable
#define ALLOW_CAN()	 				(LATDbits.LATD7				=LOW)				//controls shutdown pin that disables MAX 3051, not the CAN module itself)
#define SHUTDOWN_CAN() 				(LATDbits.LATD7				=HIGH)

//TX and RX pin define
#define TX_CAN						LATFbits.LATF1									//Define pin 59 as the transmit pin on PIC (not entirely necessary)
#define RX_CAN						LATFbits.LATF0									//Define pin 58 as the receive pin on PIC (not entirely necessary)

#define MASK						0x0000											//CAN mask (want to receive all messages)
#define FILTER						0x0000  										//CAN filter

//CAN modes
#define CAN_CONFIG_MODE()			(C1CONbits.REQOP			=0b100)				//turn on CAN configuration mode on
#define CAN_NORMAL_MODE()			(C1CONbits.REQOP			=0b000)	
#define	CAN_DISABLE()				(C1CONbits.ON				=0)					//disable CAN module
#define CAN_ENABLE()				(C1CONbits.ON				=1)					//enable CAN module		

//CAN module settings
#define FILTER_BUS_WAKE_UP()		(C1CFGbits.WAKFIL			=1)					//use CAN bus line filter for wake up
#define BUS_SAMPLE()				(C1CFGbits.SAM				=1)					//bus line is sampled three times at the sample point
#define PHASE_BUFFER()				(C1CFGbits.SEG1PH			=0b111)				//phase buffer segment 1 bits with a length of 8*Tq (must be more than 7*Tq); Tq if the time quantum
#define PROP_TIME()					(C1CFGbits.PRSEG			=0b111)				//propagation time segment bits with a length of 8*Tq (must be more than 7*Tq)
#define BAUD_RATE()					(C1CFGbits.BRP				=0b111111)			//baud rate prescaler bits Tq=(2*64)/Fsys (can be changed, was not sure what should be set)

//INterrupt settings
#define CAN_INTERRUPT_SET()			(C1INT						=0xB0020000)		//sets interrupt preferences for CAN. sets for invalid message, bus error,system error, and receive buffer
#define	INTERRUPT_VECTOR_CAN				0x2E											//interrupt vetor for CAN 1
#define	CAN_INT_ENABLED()			(IEC1bits.CAN1IE			=1)					//enables interrupt for CAN 1
#define INT_PRIORITY_LEVEL_CAN()	(IPC11bits.CAN1IP			=0x5)				//priority levels for CAN set (arbitrarily since currently only have few other interrupts enabled 
#define INT_SUB_PRIOR_LEVEL_CAN() 	(IPC11bits.CAN1IS			=0x2)

//Flag clearing macros
#define CLEAR_FLAG_SYSTEM_ERROR()	(C1INTbits.SERRIF			=0)					//macros for clearing CAN interrupt bits
#define CLEAR_FLAG_BUS_ERROR()		(C1INTbits.CERRIF			=0)
#define CLEAR_FLAG_RECEIVE()		(C1INTbits.RBIF				=0)
#define CLEAR_FLAG_CAN()			(IFS1bits.CAN1IF			=0)					//clears interrupt for all CAN interrupts, need to do this every time as well as other flag clears

//debugging tools
#define TRANSMIT_ERROR				C1TRECbits.TERRCNT								//transmitter in error state warning (when between 96 and 128)
#define RECEIVE_ERROR				C1TRECbits.RERRCNT								//receiver in error state warning (when between 96 and 128)

//FIFO configuration options
#define MSG_PER_CHANNEL				0b11111											//sets to 32 messages deep; C1FIFOCONnbits.FSIZE where 'n' is the FIFO number (0-32);will need to set separately for each FIFO
#define SET_RECEIVE					0												//C1FIFOCONnbits.TXEN	
#define SET_TRANSMIT				1											

#define REQUEST_TX()					(C1FIFOCON1bits.TXREQ		=1)




//==================================================================================================
// TYPEDEFS
//--------------------------------------------------------------------------------------------------

//message transmit buffer data structures (found and modified from PIC32 Family Reference Manual)

// Create CMSGSID  
typedef struct
{
	unsigned SID:11;
	unsigned :21;
}cmsgsid;

// Create CMSGEID 
typedef struct
{
	uint16_t DLC:4;
	uint16_t RB0:1;
	uint16_t :3;
	uint16_t RB1:1;
	uint16_t RTR:1;
	uint32_t EID:18;
	uint16_t IDE:1;
	uint16_t SRR:1;
	uint16_t :2;
}cmsgeid;

// Create CMSGDATA0 
typedef struct
{
	uint16_t Byte0:8;
	uint16_t Byte1:8;
	uint16_t Byte2:8;
	uint16_t Byte3:8;
}cmsgdata0;

//Create CMSGDATA1
typedef  struct
{
	uint16_t Byte4:8;
	uint16_t Byte5:8;
	uint16_t Byte6:8;
	uint16_t Byte7:8;
}cmsgdata1;

//main data structure
typedef union uCANTxMessageBuffer {
	struct
	{
		cmsgsid CMSGSID;
		cmsgeid CMSGEID;
		cmsgdata0 CMSGDATA0;
		cmsgdata0 CMSGDATA1;
	};
	int Word[4];
}TxMessageBuffer;



// Create CMSGDATA0 
typedef struct
{
	uint16_t Byte0:8;
	uint16_t Byte1:8;
	uint16_t Byte2:8;
	uint16_t Byte3:8;
}rxcmsgdata0;

//Create CMSGDATA1
typedef  struct
{
	uint16_t Byte4:8;
	uint16_t Byte5:8;
	uint16_t Byte6:8;
	uint16_t Byte7:8;
}rxcmsgdata1;

//main data structure
typedef union uCANRxMessageBuffer {
	struct
	{
		rxcmsgdata0 CMSGDATA0;
		rxcmsgdata0 CMSGDATA1;
	};
	int Message[2];
}RxMessageBuffer;

//==================================================================================================
// FUNCTIONS
//--------------------------------------------------------------------------------------------------

void Init_Can();
int Hut_Can();


#endif
////////////////////////////////////////////////////////////////////////////////////////////////////
//!	@file
//!	
//!	@brief			This file contains the core of the can to async application. It manages invoking 
//!					standard-specific functions [ex: uart, rs232, rs485 half-duplex, rs485 full-duplex].
//!	
//!	@author			
////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef RS485_FULL_DUPLEX_H
#define RS485_FULL_DUPLEX_H


//==================================================================================================
// INCLUDES
//--------------------------------------------------------------------------------------------------

#include "core/can_to_async.h"

//==================================================================================================
// DEFINES
//--------------------------------------------------------------------------------------------------\

//Pin configuration on PIC32

//GEN_MODE_SWITCH:			switch between RS232 and RS485
//RECEIVER_ENABLE:			logic low enables reivers, logic high disables receivers
//DRIVER_ENABLE:			logic high enables drivers, logic low disables drivers
//TE485:					logic high enables 120 ohm resistor between pins A&B and Y&Z
//LOOPBACK:					logic high enables loopback mode, logic low disables
//RECEIVER_OUTPUT:			differential receiver output for RS485
//DRIVER_INPUT:				differential driver input for RS485
//FAST_ENABLE:				in fast mode DC-DC converter is active independent of driver,receiver, and termination states
//HALF_FULL_SWITCH:			switches between half and full duplex



//#define SET_TO_RS485()					LATBbits.LATB15				=HIGH			//these are both for resistor indication of mode (application layer)
//#define SET_TO_FULL_DUPLEX()				LATBbits.LATB14				=LOW




//==================================================================================================
// FUNCTIONS
//--------------------------------------------------------------------------------------------------

void RS485_Driver_On();
void RS485_Receiver_On();
void RS485_Termination_Enabled();
void RS485_Full_Duplex();
void RS485_Loopback();
void RS485_Low_Power_Shutdown();

int Hut_RS485_FD();

#endif
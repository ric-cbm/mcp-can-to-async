////////////////////////////////////////////////////////////////////////////////////////////////////
//!	@file
//!	
//!	@brief			This file contains the core of the can to async application. It manages invoking 
//!					standard-specific functions [ex: uart, rs232, rs485 half-duplex, rs485 full-duplex].
//!	
//!	@author			
////////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef UART_H
#define UART_H



//==================================================================================================
// INCLUDES
//--------------------------------------------------------------------------------------------------

#include "core/can_to_async.h"


//==================================================================================================
// DEFINES
//--------------------------------------------------------------------------------------------------\

//UART pin configuration on PIC32

#define ENABLE_UART1()					(U1MODE						=0x8085)					//MODE register: enable UART1,enable wake-up on start bit,set to 8 data bits and odd parity, set 2 stop bits
#define TX_UART							U1TXREG
#define RX_UART							U1RXREG									
#define ENABLE_INTERRUPT_UART1()		(IEC0bits.U1RXIE			=1)						// enable interrupt of UART1 receiver
#define INTERRUPT_PRIORITY_1()			(IPC6bits.U1IP				=5)						//set priority of interrupt
#define UART1_INTERRUPT_VECTOR			24		 		
#define PARITY_STATUS_1					U1STAbits.U1STA3

#define SET_BAUD_RATE_1()				(U1BRG						=BAUD_UART)				//	uart baud eqn:	((40000000L/(16*(115200L)))-1=21	*assumes peripheral clock of  40 Mhz

//==================================================================================================
// TYPEDEFS
//--------------------------------------------------------------------------------------------------






//==================================================================================================
// FUNCTIONS
//--------------------------------------------------------------------------------------------------/*++****************************************************************************

void Uart_Init();
int Hut_Uart();


#endif

////////////////////////////////////////////////////////////////////////////////////////////////////
//!	@file			can_to_async.h 
//!	
//!	@brief			Header file for main source application file. defines macros used throughout
//! 				the application which set inputs and outputs and assign names to commonly used
//!					register locations. 
//!	
//!	@author			
////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef CAN_TO_ASYNC_H
#define CAN_TO_ASYNC_H

//==================================================================================================
// INCLUDES
//--------------------------------------------------------------------------------------------------
#include <p32xxxx.h>
#include <plib.h>																								// Provides access to the PDCP library.


//==================================================================================================
// GENERAL DEFINITIONS && MACROS
//--------------------------------------------------------------------------------------------------
// Application status variables:
#define	RC_SUCCESS					0x00								//Return code used to indicate 'SUCCESS'.
#define	RC_INVALID_PARAMETER		0x01								//Return code used to indicate an 'INVALID PARAMETER' was received in a function.

#define cpuFreq						80000000L

// General pin definitions  
#define INPUT 						1
#define OUTPUT 						0

#define HIGH						1
#define LOW 						0

//Interrupt configuration
//#define INTERRUPT_CONTROL			(INTCONbits.INTCON12		=HIGH)	//set interrupt to multi-vector mode
#define BAUD_UART					21									//uart baud eqn:	((40000000L/(16*(115200L)))-1	*assumes peripheral clock of  40 Mhz

// LED pin configuration
#define PIN_RED_OUTPUT()			(TRISEbits.TRISE2		=0)
#define PIN_GREEN_OUPUT()			(TRISEbits.TRISE3		=0)


#define LED_RED						LATEbits.LATE2
#define LED_GREEN					LATEbits.LATE3

#define LED_RED_ON()				(LED_RED				=1)
#define LED_GREEN_ON()				(LED_GREEN				=1)

#define LED_RED_OFF()				(LED_RED				=0)
#define LED_GREEN_OFF()				(LED_GREEN				=0)

#define LED_RED_TOGGLE()			(PORTEbits.RE2				=!PORTEbits.RE2)
#define LED_GREEN_TOGGLE()			(PORTEbits.RE3				=!PORTEbits.RE3)


//LTC2870 pin configuration
#define ENABLE_UART3()				(U3MODE					=0x8085)	//enable UART3,enable wake-up on start bit,set to 8 data bits and odd parity, set 2 stop bits

#define PIN_GEN_MODE_SWITCH()		(TRISBbits.TRISB1		=OUTPUT)	//switch between RS232 and RS485
#define PIN_RECEIVER_ENABLE()		(TRISBbits.TRISB2		=OUTPUT)	//logic low enables reivers, logic high disables receivers
#define PIN_DRIVER_ENABLE()			(TRISBbits.TRISB3		=OUTPUT)	//logic high enables drivers, logic low disables drivers
#define PIN_TE485()					(TRISBbits.TRISB9		=OUTPUT)	//logic high enables 120 ohm resistor between pins A&B and Y&Z
#define PIN_LOOPBACK()				(TRISBbits.TRISB11		=OUTPUT)	//logic high enables loopback mode, logic low disables
#define PIN_RECEIVER_OUTPUT()		(TRISGbits.TRISG7		=INPUT)		//differential receiver output for RS485, receiver output 1 for RS232
#define PIN_DRIVER_INPUT()			(TRISGbits.TRISG8		=OUTPUT)	//differential driver input for RS485,driver input 1 for RS232
#define PIN_FAST_ENABLE()			(TRISGbits.TRISG6		=OUTPUT)	//in fast mode DC-DC converter is active independent of driver,receiver, and termination states
#define PIN_HALF_FULL_SWITCH()		(TRISBbits.TRISB10		=OUTPUT)	//switches between half and full duplex



#define GEN_MODE_SWITCH				LATBbits.LATB1						//All of these pins correspond to pins on the LTC2870. descriptions above
#define RECEIVER_ENABLE				LATBbits.LATB2
#define DRIVER_ENABLE				LATBbits.LATB3	
#define TE485						LATBbits.LATB9
#define LOOPBACK					LATBbits.LATB11
#define RECEIVER_OUTPUT				U3RXREG
#define DRIVER_INPUT				U3TXREG
#define FAST_ENABLE					LATGbits.LATG6
#define HALF_FULL_SWITCH			LATBbits.LATB10


//CAN pin configuration
#define SET_TX_CAN_OUTPUT()			(TRISFbits.TRISF1		=OUTPUT)		//sets transmit pin to output
#define SET_RX_CAN_INPUT()			(TRISFbits.TRISF0		=INPUT)		//sets receive pin to input
#define CAN_SHUTDOWN_PIN()			(TRISDbits.TRISD7		=OUTPUT)		//sets chutdown pin to output
																		//	(will shut down CAN driver)

//UART pin configuration
#define SET_TX_UART_OUTPUT()			(TRISDbits.TRISD3		=OUTPUT)
#define SET_RX_UART_INPUT()			(TRISDbits.TRISD2		=INPUT)



//==================================================================================================
// FUNCTION PROTOTYPES
//--------------------------------------------------------------------------------------------------
uint32_t AppInit(uint32_t sysClk);
uint32_t AppRun();
void Mode_Switch();
void Initialize_Hardware();

#endif	// CAN_TO_ASYNC_H
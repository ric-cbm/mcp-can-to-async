////////////////////////////////////////////////////////////////////////////////////////////////////
//! @file RS485_full_duplex
//!
//! @details 	functions for various mode setting for RS485 full duplex, HUT function and associated 
//!				interrupt handler	
//!
//! @author		
////////////////////////////////////////////////////////////////////////////////////////////////////

#include "lib_cbm_c32.h"
#include "modules/RS485_full_duplex.h"
#include "modules/RS232.h"
#include "timer/alarm_clock.h"

//==================================================================================================
// PRIVATE FUNCTION PROTOTYPES
//--------------------------------------------------------------------------------------------------

PRIVATE void Indicate_Failure();
PRIVATE void Indicate_Success();
PRIVATE void STATUS_UART3_R485FD();

//==================================================================================================
// VARIABLES
//--------------------------------------------------------------------------------------------------

PRIVATE int m_package;			//private global variable
PRIVATE AlarmClock m_ac;		//used for alarm clock to delay LEDs
PRIVATE uint32_t m_time=250;

//==================================================================================================
// PRIVATE FUNCTIONS
//--------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief		Used to set Status register for UART3		
//!	
//!	@details 	Disable interrupt flag for transmit buffer, transmit idle state logic 1,receiver 
//!				enabled, transmitter enabled,receiver buffer interrupt flag
////////////////////////////////////////////////////////////////////////////////////////////////////
PRIVATE void STATUS_UART3_R485FD()		
{
	U3STAbits.UTXISEL=0b11;
	U3STAbits.UTXINV=0;
	U3STAbits.URXEN=1;
	U3STAbits.UTXEN=1;
	U3STAbits.URXISEL=0b10;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief	Used to indicate failure of transmission or reception of data.		
//!	
//!	@details blinks red LED three times, once every 0.25 seconds
//!			
////////////////////////////////////////////////////////////////////////////////////////////////////
//indicate failure with red LED
PRIVATE void Indicate_Failure()
{
	AlarmClock_InitMs(&m_ac,m_time);
	uint8_t z;
	for (z=0; z<6; z++)
	{
		if  (AlarmClock_IsTriggered(&m_ac))
		{
			LED_RED_TOGGLE();
			AlarmClock_InitMs(&m_ac,m_time);
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief	Used to indicate success of transmission or reception of data.		
//!	
//!	@details blinks green LED three times, once every 0.25 seconds
//!			
////////////////////////////////////////////////////////////////////////////////////////////////////
//indicate success with green LED
PRIVATE void Indicate_Success()
{
	AlarmClock_InitMs(&m_ac,m_time);
	uint8_t x;
	for (x=0; x<6; x++)
	{
		if  (AlarmClock_IsTriggered(&m_ac))
		{
			LED_GREEN_TOGGLE();
			AlarmClock_InitMs(&m_ac,m_time);
		}
	}
}


//==================================================================================================
// PUBLIC FUNCTIONS
//--------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief			RS232 Drivers On
//!	
//!	@details		Fast mode disabled,in RS232, receiver enabled,driver disabled, RS485 termination 
//!					disabled, DC-DC converter off
//!					
//!	
//!	@returns		
////////////////////////////////////////////////////////////////////////////////////////////////////	
void RS485_Driver_On()
{
	GEN_MODE_SWITCH			=HIGH;
	DRIVER_ENABLE			=HIGH;
	LOOPBACK				=LOW;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief			RS485 Receivers On
//!	
//!	@details		Fast mode disabled,in RS232, receiver enabled,driver disabled, RS485 termination 
//!					disabled, DC-DC converter off
//!					
//!	
//!	@returns		
////////////////////////////////////////////////////////////////////////////////////////////////////
void RS485_Receiver_On()
{
	GEN_MODE_SWITCH			=HIGH;
	RECEIVER_ENABLE			=LOW;
	LOOPBACK				=LOW;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief			
//!	
//!	@details		
//!					
//!	
//!	@returns		
////////////////////////////////////////////////////////////////////////////////////////////////////
void RS485_Termination_Enabled()
{
	GEN_MODE_SWITCH			=HIGH;
	TE485					=HIGH;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief			
//!	
//!	@details		
//!					
//!	
//!	@returns		
////////////////////////////////////////////////////////////////////////////////////////////////////
void RS485_Full_Duplex()
{
	GEN_MODE_SWITCH			=HIGH;
	HALF_FULL_SWITCH		=LOW;
	LOOPBACK				=LOW;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief			Loopback mode used for diagnostics, routes driver input logic levels to the 
//!					receiver output pins
//!	
//!	@details		Fast mode disabled,in RS232, receiver enabled,driver disabled, RS485 termination 
//!					disabled, DC-DC converter off
//!					
//!	
//!	@returns		
////////////////////////////////////////////////////////////////////////////////////////////////////
void RS485_Loopback()
{
	GEN_MODE_SWITCH			=HIGH;
	RECEIVER_ENABLE			=LOW;
	LOOPBACK				=HIGH;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief			Low Power Shutdown:All Main Functions Off
//!	
//!	@details		Fast mode disabled,in RS232, receiver enabled,driver disabled, RS485 termination 
//!					disabled, DC-DC converter off
//!					
//!	
//!	@returns		
////////////////////////////////////////////////////////////////////////////////////////////////////	
void RS485_Low_Power_Shutdown()
{
	FAST_ENABLE				=LOW;
	RECEIVER_ENABLE			=HIGH;
	DRIVER_ENABLE			=LOW;
	TE485					=LOW;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief		HUT for RS485 Full Duplex testing 		
//!	
//!	@details 	Initializes LTC2870 for RS485 full duplex mode, and loads Tx buffer with message (m_package), then
//!				indicates success if message is sent.
//!						*Interrupt should occur when message is received
//!						*After interrupt message should be itterated by 1 and transmitted again
//!						*message itterated 6 times before completing the loop and switching modes
//!			
////////////////////////////////////////////////////////////////////////////////////////////////////
int Hut_RS485_FD()
{
	//initialize RS485
	void RS485_Driver_On();
	void RS485_Receiver_On();
	void RS485_Full_Duplex();

	ENABLE_UART3();
	void STATUS_UART3_R485FD();
	SET_BAUD_RATE_3();
	ENABLE_INTERRUPT_UART3();
	INTERRUPT_PRIORITY_3();
	
	//Send signal out by writing to transmit buffer
	m_package=0b00000001;
	while (m_package<0b00000111) 
	{
		DRIVER_INPUT=m_package;			//transmit data package "i"
		void Indicate_Success();		//blink green LED to signal transmission			
								//Interrupt "R485FD_Receive_Check(void)" should be triggered here
	}	

	return RC_SUCCESS;
}



//==================================================================================================
// INTERUPTS/CALLBACKS
//--------------------------------------------------------------------------------------------------

// process received uart bytes
void __ISR(UART3_INTERRUPT_VECTOR, ipl5) R485FD_Receive_Check(void)

{
	//read signal from receiver output, blink green if good then increment
	if (RECEIVER_OUTPUT==m_package)
	{
		//indicate success
		void Indicate_Success();
		m_package++;
	}
	else		//accounts for all other error, blink red ten times then return to start of while loop
	{
		//indicate failure with red LED
		void Indicate_Failure();
	}
	
	// clear the software interrupt request flag
	IFS1bits.U3RXIF=0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//! @file			CAN.c
//!
//! @details		Contains initialization of CAN module, HUT function for CAN testing, and the CAN
//!					interrupt handler.
//!
//! @author		
////////////////////////////////////////////////////////////////////////////////////////////////////

#include "lib_cbm_c32.h"
#include "modules/CAN.h"
#include "timer/alarm_clock.h"

//==================================================================================================
// PRIVATE FUNCTION PROTOTYPES
//--------------------------------------------------------------------------------------------------

PRIVATE void Indicate_Failure();
PRIVATE void Indicate_Success();

//==================================================================================================
// VARIABLES
//--------------------------------------------------------------------------------------------------

PRIVATE int m_package;			//private global variable
PRIVATE AlarmClock m_ac;
PRIVATE uint32_t m_time=250;


//==================================================================================================
// PRIVATE FUNCTIONS
//--------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief	Used to indicate failure of transmission or reception of data.		
//!	
//!	@details blinks red LED three times, once every 0.25 seconds
//!			
////////////////////////////////////////////////////////////////////////////////////////////////////
//indicate failure with red LED
PRIVATE void Indicate_Failure()
{
	AlarmClock_InitMs(&m_ac,m_time);
	uint8_t y;
	for (y=0; y<6; y++)
	{
		if  (AlarmClock_IsTriggered(&m_ac))
		{
			LED_RED_TOGGLE();
			AlarmClock_InitMs(&m_ac,m_time);
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief	Used to indicate success of transmission or reception of data.		
//!	
//!	@details blinks green LED three times, once every 0.25 seconds
//!			
////////////////////////////////////////////////////////////////////////////////////////////////////
//indicate success with green LED
PRIVATE void Indicate_Success()
{
	AlarmClock_InitMs(&m_ac,m_time);
	uint8_t x;
	for (x=0; x<6; x++)
	{
		if  (AlarmClock_IsTriggered(&m_ac))
		{
			LED_GREEN_TOGGLE();
			AlarmClock_InitMs(&m_ac,m_time);
		}
	}
}
//==================================================================================================
// PUBLIC FUNCTIONS
//--------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief	function used to set input and output pins on PIC		
//!	
//!	@details utilized macros defined in header file to set PIC32 pins
//!			
////////////////////////////////////////////////////////////////////////////////////////////////////
void Init_Can()
{
	//general configurations (see CAN.h for specific actions of each)
	ALLOW_CAN();			//releases hold of shutdown pin so MAX3051 can communicate with PIC
	CAN_CONFIG_MODE();		//set can module to config mode
	CAN_DISABLE();			//disable can module to configure
	FILTER_BUS_WAKE_UP();
	BUS_SAMPLE();
	PHASE_BUFFER();
	PROP_TIME();
	BAUD_RATE();
	CAN_INTERRUPT_SET();
	CAN_INT_ENABLED();
	INT_PRIORITY_LEVEL_CAN();
	INT_SUB_PRIOR_LEVEL_CAN();
	CAN_Filter_Setup m_filters[] = {{MASK, FILTER}};		//from can.c in lib_cbm_c32. sets mask and filter to accept all

	//init one Tx FIFO and one Rx FIFO (FIFO 0 is receive, FIFO 1 is transmit)
	C1FIFOCON0bits.FSIZE			=MSG_PER_CHANNEL;
	C1FIFOCON0bits.TXEN				=SET_RECEIVE;
	C1FIFOCON0bits.DONLY			=1;					//sets only data saved. need to be in receive mode

	C1FIFOCON1bits.FSIZE			=MSG_PER_CHANNEL;
	C1FIFOCON1bits.TXEN				=SET_TRANSMIT;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief		HUT for CAN testing 		
//!	
//!	@details 	Sets CAN to normal mode and loads Tx buffer with message (m_package), then requests
//!				to transmit the message and indicates success if message is sent.
//!						*Interrupt should occur when message is received
//!						*After intterupt message should be itterated by 1 and transmitted again
//!						*message itterated 6 times before completing the loop and switching modes
//!			
////////////////////////////////////////////////////////////////////////////////////////////////////
int Hut_Can()
{ 
	int rc;
	rc=RC_SUCCESS;

	CAN_NORMAL_MODE();

	//TxMessageBuffer *CurrentMessageBuffer;

	m_package=0x01;

	while (m_package<0x07)
	{
		TxMessageBuffer *CurrentMessageBuffer;
		CurrentMessageBuffer=C1FIFOUA1;
		
		//clear message buffer
		CurrentMessageBuffer->Word[0] = 0;
		CurrentMessageBuffer->Word[1] = 0;
		CurrentMessageBuffer->Word[2] = 0;
		CurrentMessageBuffer->Word[3] = 0;


		CurrentMessageBuffer->CMSGSID.SID = 0x0;						//loads message structure and contents into FIFO transmit buffer
		CurrentMessageBuffer->CMSGEID.DLC = 0x2;
		CurrentMessageBuffer->CMSGDATA0.Byte0 = m_package;
		CurrentMessageBuffer->CMSGDATA0.Byte1 = m_package;			

		REQUEST_TX();										//requests to send message, should trigger an interrupt and go to interrupt handler
		void Indicate_Success();
	return rc;
	}
}


//==================================================================================================
// INTERUPTS/CALLBACKS
//--------------------------------------------------------------------------------------------------

void __ISR(INTERRUPT_VECTOR_CAN, ipl3) CANInterruptHandler(void)
{
	if (C1FIFOINT0bits.RXFULLIF==1)
	{
		//use C1FIFOA0 to read address where next message can be read from (gives pointer)
		int MessageOne;
		int MessageTwo;
		RxMessageBuffer * MessageBuffer;
		MessageBuffer=C1FIFOUA0;
		
		//Assign the received messages to "MessageOne" and "MessageTwo"
		MessageOne=MessageBuffer->Message[0];
		MessageTwo=MessageBuffer->Message[1];
 
		//check for success: do "MessageOne" and "MessageTwo" match the sent message (m_package)?
		if (MessageOne==m_package && MessageTwo==m_package)
		{
			//indicate success with green LED
			void Indicate_Success();
			//increment global variable to start transmit loop again
			m_package+=1;
			CLEAR_FLAG_RECEIVE();
		}
		else
		{
			//indicate failure with red LED
			void Indicate_Failure();
			CLEAR_FLAG_RECEIVE();
		}
		
	}	
	else if (C1VECbits.ICODE==0b1000001)	// check for bus error
	{
		//indicate failure
		void Indicate_Failure();
		CLEAR_FLAG_BUS_ERROR();
	}

	else if (C1VECbits.ICODE==0b1000101)	// check for system error
	{
		//indicate failure
		void Indicate_Failure();
		CLEAR_FLAG_SYSTEM_ERROR();
	}
	else (C1VECbits.ICODE==0b1000100);	//check for other system error
	{
		//indicate failure
		void Indicate_Failure();
		CLEAR_FLAG_SYSTEM_ERROR();
	}
	CLEAR_FLAG_CAN();
}


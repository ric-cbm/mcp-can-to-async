////////////////////////////////////////////////////////////////////////////////////////////////////
//! @file		RS485_half_duplex
//!
//! @details 	functions for various mode setting for RS485 half duplex, HUT function and associated 
//!				interrupt handler	
//!
//! @author		
////////////////////////////////////////////////////////////////////////////////////////////////////

#include "lib_cbm_c32.h"
#include "modules/RS485_half_duplex.h"
#include "modules/RS485_full_duplex.h"
#include "modules/RS232.h"
#include "timer/alarm_clock.h"


//==================================================================================================
// PRIVATE FUNCTION PROTOTYPES
//--------------------------------------------------------------------------------------------------
PRIVATE void Indicate_Success();
PRIVATE void Indicate_Failure();
PRIVATE void STATUS_UART3_R485HD();

//==================================================================================================
// VARIABLES
//--------------------------------------------------------------------------------------------------

PRIVATE int m_package;			//private global variable
PRIVATE AlarmClock m_ac;		//used for alarm clock to delay LEDs
PRIVATE uint32_t m_time=250;	//so will flash every 0.25 sec


//==================================================================================================
// PRIVATE FUNCTIONS
//--------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief		Used to set Status register for UART3		
//!	
//!	@details 	Disable interrupt flag for transmit buffer, transmit idle state logic 1,receiver 
//!				enabled, transmitter enabled,receiver buffer interrupt flag
////////////////////////////////////////////////////////////////////////////////////////////////////
PRIVATE void STATUS_UART3_R485HD()		
{
	U3STAbits.UTXISEL=0b11;
	U3STAbits.UTXINV=0;
	U3STAbits.URXEN=1;
	U3STAbits.UTXEN=1;
	U3STAbits.URXISEL=0b10;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief	Used to indicate failure of transmission or reception of data.		
//!	
//!	@details blinks red LED three times, once every 0.25 seconds
//!			
////////////////////////////////////////////////////////////////////////////////////////////////////
//indicate failure with red LED
PRIVATE void Indicate_Failure()
{
	AlarmClock_InitMs(&m_ac,m_time);
	uint8_t y;
	for (y=0; y<6; y++)
	{
		if  (AlarmClock_IsTriggered(&m_ac))
		{
			LED_RED_TOGGLE();
			AlarmClock_InitMs(&m_ac,m_time);
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief	Used to indicate success of transmission or reception of data.		
//!	
//!	@details blinks green LED three times, once every 0.25 seconds
//!			
////////////////////////////////////////////////////////////////////////////////////////////////////
//indicate success with green LED
PRIVATE void Indicate_Success()
{
	AlarmClock_InitMs(&m_ac,m_time);
	uint8_t x;
	for (x=0; x<6; x++)
	{
		if  (AlarmClock_IsTriggered(&m_ac))
		{
			LED_GREEN_TOGGLE();
			AlarmClock_InitMs(&m_ac,m_time);
		}
	}
}
//==================================================================================================
// PUBLIC FUNCTIONS
//--------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief			Low Power Shutdown:All Main Functions Off
//!	
//!	@details		Fast mode disabled,in RS232, receiver enabled,driver disabled, RS485 termination 
//!					disabled, DC-DC converter off
//!					
//!	
//!	@returns		
////////////////////////////////////////////////////////////////////////////////////////////////////	
void RS485_Half_Duplex()
{
	GEN_MODE_SWITCH			=HIGH;
	HALF_FULL_SWITCH		=HIGH;
	LOOPBACK				=LOW;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief			Low Power Shutdown:All Main Functions Off
//!	
//!	@details		Fast mode disabled,in RS232, receiver enabled,driver disabled, RS485 termination 
//!					disabled, DC-DC converter off
//!					
//!	
//!	@returns		
////////////////////////////////////////////////////////////////////////////////////////////////////
int Hut_RS485_HD()
{
	//initialize RS485
	void RS485_Driver_On();
	void RS485_Receiver_On();
	void RS485_Half_Duplex();

	ENABLE_UART3();
	void STATUS_UART3_RS485HD();
	SET_BAUD_RATE_3();
	ENABLE_INTERRUPT_UART3();
	INTERRUPT_PRIORITY_3();
	
	//Send signal out by writing to transmit buffer
	m_package=0b00000001;
	while (m_package<0b00000111) 
	{
		DRIVER_INPUT=m_package;			//transmit data package "i"
		void Indicate_Success();		//blink green LED to signal transmission			
										//Interrupt "R485HD_Receive_Check(void)" should be triggered here
	}	

	return RC_SUCCESS;
}



//==================================================================================================
// INTERUPTS/CALLBACKS
//--------------------------------------------------------------------------------------------------

// process received uart bytes
void __ISR(UART3_INTERRUPT_VECTOR, ipl5) R485HD_Receive_Check(void)

{
	//read signal from receiver output, blink green if good then increment
	if (RECEIVER_OUTPUT==m_package)
	{
		//indicate success
		void Indicate_Success();
		m_package++;
	}
	else		//accounts for all other error, blink red then return to start of while loop
	{
		//indicate failure with red LED
		void Indicate_Failure();
	}
	
	// clear the software interrupt request flag
	IFS1bits.U3RXIF=0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//! @file
//!
//! @details	
//!
//! @author		
////////////////////////////////////////////////////////////////////////////////////////////////////

#include "lib_cbm_c32.h" 
#include "modules/UART.h"
#include "timer/alarm_clock.h"


//==================================================================================================
// PRIVATE FUNCTION PROTOTYPES
//--------------------------------------------------------------------------------------------------

PRIVATE void Indicate_Failure();
PRIVATE void Indicate_Success();
PRIVATE void STATUS_UART3_UART1();

//==================================================================================================
// VARIABLES
//--------------------------------------------------------------------------------------------------

PRIVATE int m_package;			//private global variable
PRIVATE AlarmClock m_ac;		//used for alarm clock to delay LEDs
PRIVATE uint32_t m_time=250;

//==================================================================================================
// PRIVATE FUNCTIONS
//--------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief		Used to set Status register for UART1		
//!	
//!	@details 	Disable interrupt flag for transmit buffer, transmit idle state logic 1,receiver 
//!				enabled, transmitter enabled,receiver buffer interrupt flag
////////////////////////////////////////////////////////////////////////////////////////////////////
PRIVATE void STATUS_UART1()	
{
	U1STAbits.UTXISEL=0b11;
	U1STAbits.UTXINV=0;
	U1STAbits.URXEN=1;
	U1STAbits.UTXEN=1;
	U1STAbits.URXISEL=0b10;
}

	
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief	Used to indicate failure of transmission or reception of data.		
//!	
//!	@details blinks red LED three times, once every 0.25 seconds
//!			
////////////////////////////////////////////////////////////////////////////////////////////////////
//indicate failure with red LED
PRIVATE void Indicate_Failure()
{
	AlarmClock_InitMs(&m_ac,m_time);
	uint8_t y;
	for (y=0; y<6; y++)
	{
		if  (AlarmClock_IsTriggered(&m_ac))
		{
			LED_RED_TOGGLE();
			AlarmClock_InitMs(&m_ac,m_time);
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief	Used to indicate success of transmission or reception of data.		
//!	
//!	@details blinks green LED three times, once every 0.25 seconds
//!			
////////////////////////////////////////////////////////////////////////////////////////////////////
//indicate success with green LED
PRIVATE void Indicate_Success()
{
	AlarmClock_InitMs(&m_ac,m_time);
	uint8_t x;
	for (x=0; x<6; x++)
	{
		if  (AlarmClock_IsTriggered(&m_ac))
		{
			LED_GREEN_TOGGLE();
			AlarmClock_InitMs(&m_ac,m_time);
		}
	}
}

//==================================================================================================
// PUBLIC FUNCTIONS
//--------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief			
//!	
//!	@details		
//!					
//!	
//!	@returns		
////////////////////////////////////////////////////////////////////////////////////////////////////	
void Uart_Init()
{
	ENABLE_UART1();
	void STATUS_UART1();
	SET_BAUD_RATE_1();
	ENABLE_INTERRUPT_UART1();
	INTERRUPT_PRIORITY_1();
}
	


////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief		HUT for UART testing 		
//!	
//!	@details 	loads Tx buffer with message (m_package), then indicates success if message is sent.
//!						*Interrupt should occur when message is received
//!						*After interrupt message should be itterated by 1 and transmitted again
//!						*message itterated 6 times before completing the loop and switching modes
//!			
////////////////////////////////////////////////////////////////////////////////////////////////////
int Hut_Uart()
{
	//Send signal out by writing to transmit buffer
	m_package=0b00000001;
	while (m_package<0b00000111) 
	{
		TX_UART=m_package;				//transmit data package "i"
		void Indicate_Success();		//blink green LED to signal transmission			
								//Interrupt "Uart1_Receive_Check(void)" should be triggered here
	}	

	return RC_SUCCESS;
}
//==================================================================================================
// INTERUPTS/CALLBACKS
//--------------------------------------------------------------------------------------------------

// process received uart bytes
void __ISR(UART1_INTERRUPT_VECTOR, ipl5) Uart_Receive_Check(void)

{
	//read signal from receiver output, blink green if good and increment
	if (RX_UART==m_package)
	{
		//indicate success
		void Indicate_Success();
		m_package++;
	}
	else		//accounts for all other error, blink red three times then return to start of while loop
	{
		//indicate failure with red LED
		void Indicate_Failure();
	}
	
	// clear the software interrupt request flag
	IFS0bits.U1EIF=0;
}


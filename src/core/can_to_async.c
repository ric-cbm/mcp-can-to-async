////////////////////////////////////////////////////////////////////////////////////////////////////
//!	@file			can_to_async.c
//!	
//!	@brief			This file contains the core of the can to async HUT applicataion. It manages invoking 
//!					standard-specific functions [ex: uart, rs232, rs485 half-duplex, rs485 full-duplex]
//!					one after the other in order to test basic functioning. Also contains functions 
//!					for initializing hardware and indicating mode switching. 
//!	
//!	@author			
////////////////////////////////////////////////////////////////////////////////////////////////////
//==================================================================================================
// INCLUDES
//--------------------------------------------------------------------------------------------------
#include "lib_cbm_c32.h"								// Provides access to all the common c32 header files.																				// Provides access to the PDCP library.
#include "core/can_to_async.h"							// header file for CAN to Async HUT 
#include "modules/UART.h"								// header file for UART
#include "modules/CAN.h"								// header file for CAN
#include "modules/RS232.h"								// header file for RS232
#include "modules/RS485_half_duplex.h"					// header file for RS485 half-duplex
#include "modules/RS485_full_duplex.h"					// header file for RS485 full-duplex
#include "timer/alarm_clock.h"


//==================================================================================================
// PUBLIC FUNCTIONS => 
//--------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief	filler function used to indicate a switch in modes for HUT 		
//!	
//!	@details Will toggle two LEDs 8 times so both red and green will light up four times, every half second	
//!			
////////////////////////////////////////////////////////////////////////////////////////////////////


void Mode_Switch()
{
	AlarmClock ac;
	uint32_t time=500;
	AlarmClock_InitMs(&ac,time);
	uint32_t z;
	for (z=0; z<8; z++)
	{
		if  (AlarmClock_IsTriggered(&ac))
		{
			LED_RED_TOGGLE();
			LED_GREEN_TOGGLE();
			AlarmClock_InitMs(&ac,time);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief	function used to set input and output pins on PIC		
//!	
//!	@details utilized macros defined in header file to set PIC32 pins
//!			
////////////////////////////////////////////////////////////////////////////////////////////////////

void Initialize_Hardware()
{
	PIN_GREEN_OUPUT();
	PIN_RED_OUTPUT();
	
	PIN_GEN_MODE_SWITCH();
	PIN_RECEIVER_ENABLE();
	PIN_DRIVER_ENABLE();
	PIN_TE485();
	PIN_LOOPBACK();
	PIN_RECEIVER_OUTPUT();
	PIN_DRIVER_INPUT();
	PIN_FAST_ENABLE();
	PIN_HALF_FULL_SWITCH();

	SET_TX_CAN_OUTPUT();
	SET_RX_CAN_INPUT();
	CAN_SHUTDOWN_PIN();

	SET_TX_UART_OUTPUT();
	SET_RX_UART_INPUT();
}

//==================================================================================================
// PUBLIC FUNCTIONS => MAIN EXECUTION FLOW
//--------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief			Initializes components used in main application.
//!	
//!	@details		Initialization includes:
//!						* PIC pin initialization
//!						* External LED initialization.
//!						* Invoking module-specific intitialization functions
//!		
//!	@param[in]		sys_clk					Current system clock frequency [in hertz] to use when 
//!											setting dependencies.
//!	
//!	@returns		Modifies and returns the global 'rc' variable.
//!	@retval			#RC_SUCCESS 			When all initialization tasks were completed 
//!											successfully.
//!	@retval			#=>1					Positive value when an error occurred.
////////////////////////////////////////////////////////////////////////////////////////////////////
uint32_t AppInit(uint32_t sys_clk)
{
	uint32_t rc;
	// Initialize application-wide error code status.
	rc = RC_SUCCESS;
	
	//Hardware Initializer: sets input/output pins
	void Initialize_Hardware();

	//initialize UART	
	void Uart_Init();

	//Turn off CAN to start
	SHUTDOWN_CAN();

	// :: EXIT :: Return status of initialization. ::
	return rc;
}
	

////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief			Main application loop. Switch between and check each mode
//!
//!	@details		General processing loop for function. Switch through modes, transmit,receive,
//!					and check in each mode then indicate proper or improper transmission.
//!	 
//!	
//!	@returns		returns rc upon completion(should be RC_SUCCESS)
////////////////////////////////////////////////////////////////////////////////////////////////////
uint32_t AppRun()
{
	uint32_t rc;
	rc=RC_SUCCESS;

	void Mode_Switch();
	int Hut_Uart();
	void Mode_Switch();
	int Hut_RS232();
	void Mode_Switch();
	int Hut_RS485_FD();
	void Mode_Switch();	
	int Hut_RS485_FD();
	void Mode_Switch();
	int Hut_Can();

	return rc;
}
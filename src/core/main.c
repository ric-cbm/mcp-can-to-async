////////////////////////////////////////////////////////////////////////////////////////////////////
//!	@file			main function loop. Contains AppInit and AppRun
//!	
//!	@brief			Sets return value for validation, runs AppInit and AppRun. returns value for 
//!	
//!	@author			
////////////////////////////////////////////////////////////////////////////////////////////////////
//==================================================================================================
// INCLUDES
//--------------------------------------------------------------------------------------------------

#include "system/HardwareProfile.h"
#include "lib_cbm_c32.h"
#include "core/can_to_async.h"
#include "timer/alarm_clock.h"


#pragma config UPLLEN   = OFF        	// USB PLL Disabled
#pragma config FUSBIDIO = OFF			// USBID pin controlled by port function
#pragma config FVBUSONIO = OFF			// VBUS controlled by port function
#pragma config FPLLMUL  = MUL_20        // PLL Multiplier
#pragma config UPLLIDIV = DIV_2         // USB PLL Input Divider
#pragma config FPLLIDIV = DIV_2         // PLL Input Divider
#pragma config FPLLODIV = DIV_1			// PLL Output Divider
#pragma config FPBDIV   = DIV_1         // Peripheral Clock divisor
#pragma config FWDTEN   = OFF           // Watchdog Timer
#pragma config WDTPS    = PS4			// Watchdog Timer Postscale
#pragma config FCKSM    = CSECME        // Clock Switching & Fail Safe Clock Monitor
#pragma config OSCIOFNC = OFF           // CLKO Enable
#pragma config POSCMOD  = EC            // Primary Oscillator
#pragma config IESO     = ON         	// Internal/External Switch-over
#pragma config FSOSCEN  = OFF           // Secondary Oscillator Enable (KLO was off)
//#pragma config FNOSC    = PRIPLL        // Oscillator Selection
#pragma config FNOSC    = FRCPLL		// Oscillator Selection
#pragma config CP       = OFF           // Code Protect
#pragma config BWP      = OFF           // Boot Flash Write Protect
#pragma config PWP      = OFF           // Program Flash Write Protect
#pragma config ICESEL   = ICS_PGx2      // ICE/ICD Comm Channel Select


#define CPU_FREQ 80000000L				// cpu clock of PIC32 = 80Mhz
										//CPU�s core timer increments once every two ticks of the SYSCLK (every 25 ns for 80 MHz)



//==================================================================================================
// MAIN APPLICATION FOR PROJECT
//--------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////
//! @brief			Main function called at power on. Initializes and starts the application.
//!	
//!	@note			Function should never exit [the 'AppRun()' function it invokes should be an 
//!					infinite loop].
//!	
//!	@returns		Should never return, but if it does it will return '1' to indicate an error.
////////////////////////////////////////////////////////////////////////////////////////////////////
int main() 
{		
	uint32_t rc=RC_SUCCESS;									// return code	

	uint32_t pbClk = InitHardware(CPU_FREQ);	// initialize the cpu hardware

	rc = AppInit(CPU_FREQ);						// call application init

	if (rc == RC_SUCCESS)
		rc = AppRun();							// call application loop
			
	return rc;
}
